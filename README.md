# crud-form-mysqli

Este projeto contém um CRUD com formulário, incluindo campo para subir imagens e com a conexão feita em MySQLi.

## Instalação

### Github

Na pasta do seu projeto, abra o terminal e digite:
```console
$ git clone https://github.com/thiagofranchin/crud-form-mysqli.git
```

Na pasta `_material` tem o banco de dados pronto para ser importado no projeto.
